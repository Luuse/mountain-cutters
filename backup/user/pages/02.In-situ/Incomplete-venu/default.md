---
text: Incomplete-venu
title: 'Incomplète venu'
admin: {  }
published: true
visible: true
media_order: venus-incompletes-middelheim-02-5.jpg
---

# Incomplète venu

![venus-incompletes-middelheim-02-5](venus-incompletes-middelheim-02-5.jpg "venus-incompletes-middelheim-02-5")

-Faire étalage, displays et autres dispositifs d'exposition, Publication Esban.

-Septembre Tiberghien, Au Creux de l'Enfer mountaincutters fait écouter la langue des pierres, The ART NEWSPAPER, septembre 2019, N.11.
