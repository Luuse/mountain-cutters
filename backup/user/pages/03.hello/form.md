---
title: about
---
mountaincutters live and work in Brussels.EXPOSITION PERSONNELLES/SOLO SHOWSSOLO EXHIBITIONS 2022

*Mondes multiples, Centre d’Art Neuchâtel, CH.
*Suprainfinit Gallery, Bucarest, RO.
*Centre d’Art Bastille, Grenoble, FR. 2021
*Les indices de la respiration primitive, La Verrière-Fondation d’entrepriseHermès, curated by Guillaume Desanges, Brussels, BE.
*The holes will be filled again, Middelheim Museum, YOUNG ARTIST FUND 2021,trio with Maika Garnica et Mostafa Saifi Rahmouni, Peter boons curator Middelheimmuseum, Antwerp, BE.
*Objets-Horizons, Art-O-Rama, Friche de la belle de mai, marseille, FR.
*Du moins côte à côte, centre d’art la médiatine, Magma-10ème Triennale,Ottignies/Louvain-La-Neuve, duo with jot fau, brussels, BE.
*Du pouce jusqu’à l’auricluaire, centre d’art espace croisé, roubaix, fr. 2020
*Le sens du sol, 29ème édition L'Art dans les Chapelles, Chapelle Saint-Meldéoc, Guern.
*Drie Handen, en duo avec Jot Fau, Encore, Bruxelles 2019
*Anatomie d'un corps absent, Le Creux De L'Enfer, Thiers
*Equation du vent zéro, Chapelle des Jésuites, ESBAN Nîmes
*Asphyxie fonctionnelle, Le Papillon, Musée du Vieux, Nîmes
*Les morceaux de paysages enrayaient l’appareil corps, Centre de Céramique Contemporain, La Borne 2018
*SPOLIA (Généalogies fictives) Guillaume Désanges et mountaincutters, Grand Café de Saint-Nazaire.
*Situare II, WONDER, PARIS.
*Perception model, BRDG Antwerpen, Antwerp 2015
* Becoming Ground, IDK Contemporary and Ping Pong Gallery, Brussels 2014
*Heures-Reliefs, galerie Art-Cade des Grands Bains Douches de la plaine,  Marseille
*Concrétions, Project Room, galerie Gourvennec Ogor, Prix ESADMM 2014,  Marseille  EXPOSITION COLLECTIVES/COLLECTIVE EXHIBITIONS  2022
*Three Tropes Of Entropy, C-MINE, Genk, BE.
*HORST, co-production Kanal-pompidou, Bruxelles, BE. 2021
*Publiek Park, Young Friends of the S.M.A.K, Ghent, BE.
*MAGMA-10eme triennale, louvain-la-neuve, curated by adrien grimmeau, BE.
*Lost & Found, curated by C.Veys & Edgard.F.Grima, Hangar Art Center, Brussels.
*Avalanche, curated by Andy Rankin & Nelson Pernisco, Pal Project, Paris, FR.
*La cité sous le ciel, curated by Sylvie Boulanger, CNEAI, Paris, FR.
*3 Collectionneurs, collection Edgard F. GRIMA, SAFFCA.EU & Olivier Gevart,Eté 78, Brussels, FR.
*La Page Manquante, curated by R.A.Dormeuil, CWB Paris, FR. 2020
*Serendipity, curated by Septembre Tiberghien, Ete 78, Brussels, BE.
*Generation brussels, curated by Evelyn Simons, Brussels Gallery Week-end Galerie Louise, Bruxelles.
*Immaterial salon, Art-O-Rama, Marseille
*Biennale Miroirs #3, Parc Enghien, commissariat Myriam Louyest et Christophe Veys.
*Signal espace(s) reciproque(s), commissariat Lola Meotti, Aurélie Faure, Panorama, Friche de la Belle de mai, CWB Paris
*Objets inanimés, La villa Henry, Circa-ip/Isabelle Pellegrini.
*A Spoonfull of Sugar, Komunuma, Collectif Diamètre.  2019-Les promesses de la matière, Villa Belleville, Paris. 2018
*Art Cade, 25 X, Marseille 2017
*Perpetual Construction : a dialogue with the house of Jean Prouvé III, CAB, Brussels
*Incartades, La déviation, Marseille
*Goodbye exhibition, Show Room, Air Antwerpen
*Visite Buissonière, commissariat Thankyouforcoming, MAMAC, Nice 2016
*In the wake of his surrounding he fades, Formcontent, Extra City, Air Antwerpen and Studio Start, Antwerp
*Terres secrètes, JEP, Voyons Voir, Puyloubier
*Détours, VoyonsVoir, Puyloubier
*61ème Salon de Montrouge, Commissariat : Ami Barak et Marie Gautier, Paris
*Trois Revues s'exposent, Pétrole éditions, Peinture Fraîche, Brussels
*The studio interrupted, AIR Antwerpen and Studio Start, Antwerp
*Poppositions, Leftovers, crumbs, IDK Contemporary, La Vallée, Brussels
*Poppositions, Contrat clé en main, Curate It Yourself, La Vallée, Brussels
*Prends dans ton sac, Terrible, Paris 2015
*Catharsis-Projection, Cinéma Aventure, Brussels, 
*Les cimes des arbres, peut-être, Galerie Iconoscope, Commisaire associé Michkaël roy, Montpellier-Des avatars qui volent aux secours de nos mensonges, LabelM, Instants Vidéo, Marseille, Béton7, Athènes
*Curate It Yourself (CIY), Villa Belleville, Paris 2014
*想 «法» / Franc, c’est, Suzhou, Chine
* Art-o-Rama, Prix ESADMM 2014, Marseille 2013
* Festival des arts éphémères 5ème édition,  Marseille Provence 2013
* les Actes En Silence dans le cadre de Marseille 2013 OFF, organisé par le labelm, Marseille
* L’île Déserte,  grands terrains,  Marseille   RESIDENCES, ETUDES ET PRIX/EDUCATION, RESIDENCIES AND PRIZE 2021
*Moly Sabata, Sablons
*Espace Croisé, Roubaix 2020
*Fondation Martell, Cognac.
*Aide à la création DRAC PACA
*Workshop, "assise incomplète" La villa Saint Clair, Beaux Art de Sète 2019
*Nominés Prix Aica, présentation Septembre Tiberghien
*Workshop “Corps de pluie”, ENSA Bourges
*Workshop “Objet Lacunaire”, ESBAN Nîmes 2017
*Résidence LaBorne, Henrichemont
*Résidence Pontivy, Art dans les chapelles 2016
*Workshop terre secrète, Voyons Voir,  Puyloubier
* Résidence, VOYONS-VOIR, Puyloubier
*Résidence, STRT KIT,  Air Antwerpen and Studio Start, Antwerp
*Workshop à l'école préparatoire des beaux-arts Sète 2015
* Aide au matériel, DRAC PACA 2014 
*Lauréat du Prix ESADMM 2014 2014
*  Diplôme National Supérieur d’Expression Plastique (DNSEP) en duo avec félicitation du jury à l’Ecole Supérieure d’Arts et de Design Marseille Méditerranée (ESADMM)   EDITIONS, ACQUISITION, AND PUBLICATIONS 2020
*Monographie, Fraeme, Marseille
*Serendipity, été 78, Bruxelles 2019-Faire étalage, displays et autres dispositifs d'exposition, Publication Esban.-Septembre Tiberghien, Au Creux de l'Enfer mountaincutters fait écouter la langue des pierres, The ART NEWSPAPER, septembre 2019, N.11. 2018-Le Chant des Ruines, Septembre Tiberghien, L'Art Même n°77-L’Humanité #22549, “Les mountaincutters, spéléologues de l’espace-temps”, PierreBarbancey-Artais Art Contemporain, “portrait de mountaincutters, autour de l’exposition SPOLIA”Guillaume Clerc-Limited Edition, Conversation with Jean Prouvé, fondation cab, galerie downtown 2017-Le Quotidien de l’Art #1345, “mountaincutters, l’invention d’une perspective”, MarionVasseur Raluy,-Limited Edition (300ex) “Le Désordre des Choses”, Guillaume Désanges, with graphicdesigner Ines Cox, STRT KIT.-H-Art Magazine #174, “La maison qui nous parle”, Catherine Angelini 2016
*H Art Magazine #156, Anne-Marie Poels, Antwerp. 2015
* Talweg O3, Pétrole éditions. 2014
* Global Damages replay, La panacée, Montpellier
* Muet, publication ESADMM, Marseille
*Vers la graine solide, Atelier Tchiekebe, Marseille  ACQUISITION
*Frac PACA
*Collections privées
*Collection Middelheimmuseum
*Collection Flemish Community
