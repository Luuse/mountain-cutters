---
title: 'mais la présence au monde 2'
media_order: 'maislapresenceaumonde2modif-1.jpg,maislapresenceaumonde2modif-2.jpg,maislapresenceaumonde2modif-3.jpg,maislapresenceaumonde2modif-4.jpg,maislapresenceaumonde2modif-5.jpg,maislapresenceaumonde2modif-6.jpg,maislapresenceaumonde2modif-7.jpg,maislapresenceaumonde2modif-8.jpg,maislapresenceaumonde2modif-9.jpg,maislapresenceaumonde2modif-10.jpg,maislapresenceaumonde2modif-11.jpg,maislapresenceaumonde2modif-12.jpg'
---

_Mais la présence au monde_, 2021.
Horst. Festival, Vilvoorde, BE
copper, blowed-glass, wool, video 3'43", steel, pastel