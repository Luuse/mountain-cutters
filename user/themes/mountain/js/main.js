window.onload = function () {
    const listItems = document.querySelectorAll("ul.filter__section li");
    const galleryItem = document.querySelectorAll(".gallery__type");

    function toggleActiveClass(t) {
        listItems.forEach((t) => {
            t.classList.remove("active");
        }),
            t.classList.add("active");
    }

    function toggleProjects(t) {
        // OPTION 1
        // Outer if/then, two inner .forEach()

        // if ("all" === t)
        //     galleryItem.forEach(item => {
        //         item.classList.remove('tag');
        //     });
        // else
        //     galleryItem.forEach(item => {
        //         item.dataset.class === t
        //             ? (item.classList.add('tag'))
        //             : (item.classList.remove('tag'))
        //             ;
        //     });

        // OPTION 2
        // One outer .forEach(), inner if/then

        galleryItem.forEach(item => {
            if ("all" === t)
                item.style.display = "block";
            else
                item.dataset.class === t
                    ? (item.style.display = "block")
                    : (item.style.display = "none");
        });
    }

    for (let t = 0; t < listItems.length; t++)
        listItems[t].addEventListener("click", function () {
            toggleActiveClass(listItems[t]),
            toggleProjects(listItems[t].dataset.class);
        });
};

function closeNav() {
    document.getElementById("mySidebar").style.display = "none";
    document.getElementById("main").style.display = "none";


}

function inSituMenu(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("show") == -1) {
    x.className += "show";
  } else {
    x.className = x.className.replace("show", "");
  }
}

function changeText(idElement) {
    var element = document.getElementById('element' + idElement);
    if (idElement === 1 || idElement === 2) {
        if (element.innerHTML === 'Index') element.innerHTML = '&#10005;';
        else {
            element.innerHTML = 'Index';
        }
    }
}
