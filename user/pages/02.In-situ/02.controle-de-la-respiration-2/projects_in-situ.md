---
title: 'contrôle de la respiration 2'
media_order: 'CAN-ctrl-c-respiration-modif-2-1.jpg,CAN-ctrl-c-respiration-modif-2-2.jpg,CAN-ctrl-c-respiration-modif-2-3.jpg,CAN-ctrl-c-respiration-modif-2-4.jpg,CAN-ctrl-c-respiration-modif-2-5.jpg,CAN-ctrl-c-respiration-modif-2-6.jpg,CAN-ctrl-c-respiration-modif-2-7.jpg,CAN-ctrl-c-respiration-modif-2-8.jpg,CAN-ctrl-c-respiration-modif-2-9.jpg,CAN-ctrl-c-respiration-modif-2-10.jpg,CAN-ctrl-c-respiration-modif-2-11.jpg,CAN-ctrl-c-respiration-modif-2-12.jpg'
visible: true
admin:
    children_display_order: collection
content: "_Contrôle de la respiration_, 2020.\nCetre d'art Neufchâtel, FR\nceramics, copper, blowed-glass, steel, wood"
images:
    CAN-ctrl-c-respiration-modif-2-9.jpg:
        name: CAN-ctrl-c-respiration-modif-2-9.jpg
        type: image/jpeg
        size: 554643
        path: CAN-ctrl-c-respiration-modif-2-9.jpg
    CAN-ctrl-c-respiration-modif-2-8.jpg:
        name: CAN-ctrl-c-respiration-modif-2-8.jpg
        type: image/jpeg
        size: 697398
        path: CAN-ctrl-c-respiration-modif-2-8.jpg
    CAN-ctrl-c-respiration-modif-2-7.jpg:
        name: CAN-ctrl-c-respiration-modif-2-7.jpg
        type: image/jpeg
        size: 650755
        path: CAN-ctrl-c-respiration-modif-2-7.jpg
    CAN-ctrl-c-respiration-modif-2-6.jpg:
        name: CAN-ctrl-c-respiration-modif-2-6.jpg
        type: image/jpeg
        size: 634012
        path: CAN-ctrl-c-respiration-modif-2-6.jpg
    CAN-ctrl-c-respiration-modif-2-5.jpg:
        name: CAN-ctrl-c-respiration-modif-2-5.jpg
        type: image/jpeg
        size: 626598
        path: CAN-ctrl-c-respiration-modif-2-5.jpg
    CAN-ctrl-c-respiration-modif-2-4.jpg:
        name: CAN-ctrl-c-respiration-modif-2-4.jpg
        type: image/jpeg
        size: 609228
        path: CAN-ctrl-c-respiration-modif-2-4.jpg
    CAN-ctrl-c-respiration-modif-2-3.jpg:
        name: CAN-ctrl-c-respiration-modif-2-3.jpg
        type: image/jpeg
        size: 573591
        path: CAN-ctrl-c-respiration-modif-2-3.jpg
    CAN-ctrl-c-respiration-modif-2-2.jpg:
        name: CAN-ctrl-c-respiration-modif-2-2.jpg
        type: image/jpeg
        size: 725077
        path: CAN-ctrl-c-respiration-modif-2-2.jpg
    CAN-ctrl-c-respiration-modif-2-12.jpg:
        name: CAN-ctrl-c-respiration-modif-2-12.jpg
        type: image/jpeg
        size: 707263
        path: CAN-ctrl-c-respiration-modif-2-12.jpg
    CAN-ctrl-c-respiration-modif-2-11.jpg:
        name: CAN-ctrl-c-respiration-modif-2-11.jpg
        type: image/jpeg
        size: 543269
        path: CAN-ctrl-c-respiration-modif-2-11.jpg
legend: '2020 | Centre d''art Neufchâtel, FR'
---

_Contrôle de la respiration, 2020._
Centre d'art Neufchâtel, FR
ceramics, copper, blowed-glass, steel, wood