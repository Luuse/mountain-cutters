---
title: 'mais la présence au monde 1'
media_order: 'maislapresenceaumonde1-1.jpg,maislapresenceaumonde1-2.jpg,maislapresenceaumonde1-3.jpg,maislapresenceaumonde1-4.jpg,maislapresenceaumonde1-5.jpg,maislapresenceaumonde1-6.jpg,maislapresenceaumonde1-7.jpg,maislapresenceaumonde1-8.jpg,maislapresenceaumonde1modif-9.jpg,maislapresenceaumonde1modif-10.jpg,maislapresenceaumonde1modif-11.jpg,maislapresenceaumonde1modif-12.jpg'
visible: true
admin:
    children_display_order: collection
content: "_Mais la présence au monde_, 2021.\nHorst. Festival, Vilvoorde, BE\ncopper, blowed-glass"
images:
    maislapresenceaumonde1-8.jpg:
        name: maislapresenceaumonde1-8.jpg
        type: image/jpeg
        size: 593059
        path: maislapresenceaumonde1-8.jpg
    maislapresenceaumonde1-7.jpg:
        name: maislapresenceaumonde1-7.jpg
        type: image/jpeg
        size: 613459
        path: maislapresenceaumonde1-7.jpg
    maislapresenceaumonde1-6.jpg:
        name: maislapresenceaumonde1-6.jpg
        type: image/jpeg
        size: 880511
        path: maislapresenceaumonde1-6.jpg
    maislapresenceaumonde1-5.jpg:
        name: maislapresenceaumonde1-5.jpg
        type: image/jpeg
        size: 675993
        path: maislapresenceaumonde1-5.jpg
    maislapresenceaumonde1-4.jpg:
        name: maislapresenceaumonde1-4.jpg
        type: image/jpeg
        size: 526624
        path: maislapresenceaumonde1-4.jpg
    maislapresenceaumonde1-3.jpg:
        name: maislapresenceaumonde1-3.jpg
        type: image/jpeg
        size: 609643
        path: maislapresenceaumonde1-3.jpg
    maislapresenceaumonde1-2.jpg:
        name: maislapresenceaumonde1-2.jpg
        type: image/jpeg
        size: 850244
        path: maislapresenceaumonde1-2.jpg
    maislapresenceaumonde1-1.jpg:
        name: maislapresenceaumonde1-1.jpg
        type: image/jpeg
        size: 861380
        path: maislapresenceaumonde1-1.jpg
    maislapresenceaumonde1modif-9.jpg:
        name: maislapresenceaumonde1modif-9.jpg
        type: image/jpeg
        size: 691227
        path: maislapresenceaumonde1modif-9.jpg
    maislapresenceaumonde1modif-12.jpg:
        name: maislapresenceaumonde1modif-12.jpg
        type: image/jpeg
        size: 797689
        path: maislapresenceaumonde1modif-12.jpg
legend: '2021 | Horst. Festival, Vilvoorde, BE'
---

_Mais la présence au monde, 2021_
Horst. Festival, Vilvoorde, BE
copper, blowed-glass, wool, video 3'43", steel, pastel