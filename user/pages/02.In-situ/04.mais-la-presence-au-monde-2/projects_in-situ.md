---
title: 'mais la présence au monde 2 '
images:
    maislapresenceaumonde2modif-9.jpg:
        name: maislapresenceaumonde2modif-9.jpg
        type: image/jpeg
        size: 610659
        path: maislapresenceaumonde2modif-9.jpg
    maislapresenceaumonde2modif-8.jpg:
        name: maislapresenceaumonde2modif-8.jpg
        type: image/jpeg
        size: 735490
        path: maislapresenceaumonde2modif-8.jpg
    maislapresenceaumonde2modif-7.jpg:
        name: maislapresenceaumonde2modif-7.jpg
        type: image/jpeg
        size: 569831
        path: maislapresenceaumonde2modif-7.jpg
    maislapresenceaumonde2modif-6.jpg:
        name: maislapresenceaumonde2modif-6.jpg
        type: image/jpeg
        size: 1107217
        path: maislapresenceaumonde2modif-6.jpg
    maislapresenceaumonde2modif-5.jpg:
        name: maislapresenceaumonde2modif-5.jpg
        type: image/jpeg
        size: 1047491
        path: maislapresenceaumonde2modif-5.jpg
    maislapresenceaumonde2modif-4.jpg:
        name: maislapresenceaumonde2modif-4.jpg
        type: image/jpeg
        size: 1448282
        path: maislapresenceaumonde2modif-4.jpg
    maislapresenceaumonde2modif-3.jpg:
        name: maislapresenceaumonde2modif-3.jpg
        type: image/jpeg
        size: 1220598
        path: maislapresenceaumonde2modif-3.jpg
    maislapresenceaumonde2modif-2.jpg:
        name: maislapresenceaumonde2modif-2.jpg
        type: image/jpeg
        size: 719367
        path: maislapresenceaumonde2modif-2.jpg
    objetincompletfev+arcamglass-11-modif.jpg:
        name: objetincompletfev+arcamglass-11-modif.jpg
        type: image/jpeg
        size: 734834
        path: objetincompletfev+arcamglass-11-modif.jpg
    ObjetIncolmplets+LESPUGUEmodif-3.jpg:
        name: ObjetIncolmplets+LESPUGUEmodif-3.jpg
        type: image/jpeg
        size: 824577
        path: ObjetIncolmplets+LESPUGUEmodif-3.jpg
content: "_Mais la présence au monde_, 2021.\nHorst. Festival, Vilvoorde, BE\nncopper, blowed-glass, wool, video 3'43\\\", steel, pastel"
legend: '2021 | Horst. Festival, Vilvoorde, BE'
---

_Mais la présence au monde, 2021._
Horst. Festival, Vilvoorde, BE
copper, blowed-glass, wool, video 3'43", steel, pastel