---
title: 'contrôle de la respiration 1'
media_order: 'CAN-ctrl-c-respiration-modif-1.jpg,CAN-ctrl-c-respiration-modif-2.jpg,CAN-ctrl-c-respiration-modif-3.jpg,CAN-ctrl-c-respiration-modif-4.jpg,CAN-ctrl-c-respiration-modif-5.jpg,CAN-ctrl-c-respiration-modif-6.jpg,CAN-ctrl-c-respiration-modif-7.jpg,CAN-ctrl-c-respiration-modif-8.jpg,CAN-ctrl-c-respiration-modif-9.jpg,CAN-ctrl-c-respiration-modif-10.jpg,CAN-ctrl-c-respiration-modif-11.jpg,CAN-ctrl-c-respiration-modif-12.jpg,CAN-ctrl-c-respiration-modif-13.jpg'
visible: '1'
admin:
    children_display_order: collection
images:
    CAN-ctrl-c-respiration-modif-9.jpg:
        name: CAN-ctrl-c-respiration-modif-9.jpg
        type: image/jpeg
        size: 578086
        path: CAN-ctrl-c-respiration-modif-9.jpg
    CAN-ctrl-c-respiration-modif-8.jpg:
        name: CAN-ctrl-c-respiration-modif-8.jpg
        type: image/jpeg
        size: 663571
        path: CAN-ctrl-c-respiration-modif-8.jpg
    CAN-ctrl-c-respiration-modif-7.jpg:
        name: CAN-ctrl-c-respiration-modif-7.jpg
        type: image/jpeg
        size: 593259
        path: CAN-ctrl-c-respiration-modif-7.jpg
    CAN-ctrl-c-respiration-modif-6.jpg:
        name: CAN-ctrl-c-respiration-modif-6.jpg
        type: image/jpeg
        size: 583294
        path: CAN-ctrl-c-respiration-modif-6.jpg
    CAN-ctrl-c-respiration-modif-5.jpg:
        name: CAN-ctrl-c-respiration-modif-5.jpg
        type: image/jpeg
        size: 579794
        path: CAN-ctrl-c-respiration-modif-5.jpg
    CAN-ctrl-c-respiration-modif-4.jpg:
        name: CAN-ctrl-c-respiration-modif-4.jpg
        type: image/jpeg
        size: 650885
        path: CAN-ctrl-c-respiration-modif-4.jpg
    CAN-ctrl-c-respiration-modif-3.jpg:
        name: CAN-ctrl-c-respiration-modif-3.jpg
        type: image/jpeg
        size: 552100
        path: CAN-ctrl-c-respiration-modif-3.jpg
    CAN-ctrl-c-respiration-modif-2.jpg:
        name: CAN-ctrl-c-respiration-modif-2.jpg
        type: image/jpeg
        size: 652434
        path: CAN-ctrl-c-respiration-modif-2.jpg
    CAN-ctrl-c-respiration-modif-13.jpg:
        name: CAN-ctrl-c-respiration-modif-13.jpg
        type: image/jpeg
        size: 662265
        path: CAN-ctrl-c-respiration-modif-13.jpg
    CAN-ctrl-c-respiration-modif-12.jpg:
        name: CAN-ctrl-c-respiration-modif-12.jpg
        type: image/jpeg
        size: 531791
        path: CAN-ctrl-c-respiration-modif-12.jpg
content: "* _Contrôle de la respiration_, 2020._\n\n\n* Centre d'art Neufchâtel, FR\n\n\n* ceramics, copper, blowed-glass, steel, wood"
text: "_Contrôle de la respiration_, 2020._\r\n\r\n> Centre d'art Neufchâtel, FR\r\n\r\n> ceramics, copper, blowed-glass, steel, wood"
body_classes: ''
order_by: ''
order_manual: ''
legend: '2020 | Centre d''art Neufchâtel, FR'
---

__Contrôle de la respiration_, 2020.__
Centre d'art Neufchâtel, FR
ceramics, copper, blowed-glass, steel, wood