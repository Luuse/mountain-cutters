---
title: About
onpage_menu: true
content:
    items: '@self.modular'
    order:
        by: default
        dir: asc
        custom:
            - _bio
            - _text
            - _contact
published: true
date: '26-01-2023 12:01'
---

