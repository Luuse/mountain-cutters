---
title: text
text: "Septembre Tiberghien, Au Creux de l'Enfer mountaincutters fait écouter la langue des pierres, The ART NEWSPAPER, septembre 2019, N.11.\nSeptembre Tiberghien, Le Chant des Ruines, L'Art Même n°77, pour l'exposition SPOLIA, Grand Café Saint-Nazaire, 2019.\nCedric Aurelle, Voyage dans l'archéologie post-industrielle à Saint-Nazaire, The Art Newspaper Daily N°179 pour l'exposition SPOLIA, Grand Café Saint-Nazaire, 2018.\nGuillaume Clerc, Le sixième acte de la tragédie, Artaïs, SPOLIA, Grand Café Saint-Nazaire, 2018.\nPierre Barbancey, mountaincutters spéléologue de l'espace-temps, l'Humanité 6.nov.2018, SPOLIA, Grand Café Saint-Nazaire.\nMarion Vasseur Raluy, L'invention d'une perspective, Quotidien de l'art n°1345, 2018.\nGuillaume Désanges, Publication, graphic designer Ines Cox, STRT kit 2016, Brussels, 2017.\nGuillaume Désanges, 61ème salon de Montrouge, Paris, 2016.\nAnne-Marie Poels, H-ART magazine #156, STRT KIT, 2016.\nLudmilla Cerveny, Becomming Ground, IDK Contemporary, Anvers, 2015.\n\n \n"
file:
    'am77-web 20lechantdesruinesstmc.jpg':
        name: 'am77-web 20lechantdesruinesstmc.jpg'
        type: image/jpeg
        size: 157305
        path: 'am77-web 20lechantdesruinesstmc.jpg'
    mountaincuttersnewspaperseptember.jpg:
        name: mountaincuttersnewspaperseptember.jpg
        type: image/jpeg
        size: 365554
        path: mountaincuttersnewspaperseptember.jpg
published: true
visible: true
media_order: 'Horst 2022_ de kracht van mooie dingen.pdf,33 Mountaincutters.pdf'
---

# Text/Press

* [Horst 2022_ de kracht van mooie dingen.pdf](Horst%202022_%20de%20kracht%20van%20mooie%20dingen.pdf?target=_blank)
* [33 Mountaincutters.pdf](33%20Mountaincutters.pdf?target=_blank)
* [Septembre Tiberghien, Le Chant des Ruines, L'Art Même n°77, pour l'exposition SPOLIA, Grand Café Saint-Nazaire, 2019.]()
* [Cedric Aurelle, Voyage dans l'archéologie post-industrielle à Saint-Nazaire, The Art Newspaper Daily N°179 pour l'exposition SPOLIA, Grand Café Saint-Nazaire, 2018]().
* Guillaume Clerc, Le sixième acte de la tragédie, Artaïs, SPOLIA, Grand Café Saint-Nazaire, 2018.
* Pierre Barbancey, mountaincutters spéléologue de l'espace-temps, l'Humanité 6.nov.2018, SPOLIA, Grand Café Saint-Nazaire.
* Marion Vasseur Raluy, L'invention d'une perspective, Quotidien de l'art n°1345, 2018.
* Guillaume Désanges, Publication, graphic designer Ines Cox, STRT kit 2016, Brussels, 2017.
* Guillaume Désanges, 61ème salon de Montrouge, Paris, 2016.
* Anne-Marie Poels, H-ART magazine#156, STRT KIT, 2016.
* Ludmilla Cerveny, Becomming Ground, IDK Contemporary, Anvers, 2015.


 
